FROM python:3.8.6-slim-buster

LABEL maintainer="sobolevn@wemake.services"
LABEL vendor="wemake.services"

ENV DOCKERIZE_VERSION=v0.6.1 \
  TINI_VERSION=v0.19.0

# System deps:
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
    build-essential \
    make \
    libpq-dev \
    # build-time dependencies:
    curl \
    wget \
  # Installing `dockerize` utility:
  # https://github.com/jwilder/dockerize
  && wget "https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" \
  && tar -C /usr/local/bin -xzvf "dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" \
  && rm "dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" && dockerize --version \
  # Installing `tini` utility:
  # https://github.com/krallin/tini
  && wget -O /usr/local/bin/tini "https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini" \
  && chmod +x /usr/local/bin/tini && tini --version \
  # Cleaning build-time dependencies:
  && apt-get remove -y curl wget \
  # Cleaning cache:
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# Copy only requirements, to cache them in docker layer
COPY ./requirements.txt /code/
WORKDIR /code
RUN pip install -r requirements.txt

# We customize how our app is loaded with the custom entrypoint:
COPY ./entrypoint.sh /docker-entrypoint.sh
RUN chmod +x '/docker-entrypoint.sh'

# Copy other sources:
COPY ./Makefile ./code.py /code/

ENTRYPOINT ["tini", "--", "/docker-entrypoint.sh"]
