# Docker example project for Gitlab

To show this example:
1. Create new Gitlab project
2. Create local `.git` project
3. Create the initial commit
4. Push it to Gitlab, the build should be fine
5. In case you want to how errors work, break something in `code.py`
6. Show how easy it is to replicate the exact setup from the CI

Example: https://gitlab.com/sobolevn/docker-project
